<?php

use Illuminate\Database\Seeder;
use App\Outlet;
use Faker\Factory as Faker;

class OutletsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
 
    	for($i = 1; $i <= 50; $i++){
 
    	      // insert data ke table pegawai menggunakan Faker
    		DB::table('outlets')->insert([
    			'code' => 'AB00'.$i,
    			'name' => 'Outlet '.$faker->name,
    			'status' => $faker->randomElement($array = array ('0','1')),
    			'address' => $faker->address,
    			'phone' => $faker->phoneNumber
    		]);
 
    	}
    }
}
